### Prerequisites
Apache HTTP Server:
```
apt-get install apache2
```
Three Apache modules:
```
sudo a2enmod cgi
sudo a2enmod rewrite
sudo a2enmod headers
```